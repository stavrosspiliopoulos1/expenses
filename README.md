How to clone this project

    clone the repo: git clone https://gitlab.com/stavrosspiliopoulos1/expenses.git

    create a virtual environment: virtualenv py -m venv project-name

    activate the virtual environment: project-name\Scripts\activate.bat

    install requirements: pip install -r requirements.txt

    To install requirements, these packages are required in system sudo apt-get install 
